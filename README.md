## Reference API Proxy 

Instructions for hello-world-api-demo


[TOC levels=1-6]: # "# Table of Contents"

# Table of Contents
- [Reference API Proxy](#reference-api-proxy)
- [Description](#description)
- [Purpose](#purpose)
- [Pre-Requisites](#pre-requisites)
- [External configuration](#external-configuration)
    - [Access token generation](#access-token-generation)
        - [Acurl and get_token](#acurl-and-get_token)
    - [Maven cicd settings xml config](#maven-cicd-settings-xml-config)
- [Usage Instructions](#usage-instructions)
- [Lifecycle for API proxy deployment](#lifecycle-for-api-proxy-deployment)
    - [Lint Proxy](#lint-proxy)
    - [Unit Tests](#unit-tests)
    - [Bundle Package](#bundle-package)
    - [Upload Caches](#upload-caches)
    - [Upload Target Servers](#upload-target-servers)
    - [Upload KVM's](#upload-kvms)
    - [Deploy Proxy](#deploy-proxy)
    - [Upload Products](#upload-products)
    - [Upload Developers](#upload-developers)
    - [Upload Apps](#upload-apps)
    - [Export Keys](#export-keys)
    - [Integration Tests](#integration-tests)
    - [Upload to Artifactory](#upload-to-artifactory)



## Description

While deploying an API proxy to any targeted Apigee Edge env, it must go through with the several CI
phases such as Linting, Code Coverage, Unit testing, Build and packaging, configuring environment
level dependencies (KVM's, Caches, Target Servers), Configuring org level entities(API products,
Developers, Apps) and integration testing. Also, artifacts must be deploy to the remote locations, i.e.
Nexus.

    
## Purpose

The DevOps engineer should be able to execute all the above mentioned CI phases by manually running
the steps.

## Pre-Requisites 
- JDK 8
- Apache Maven
- Linux/WSL

## Usage Instructions

This Api Proxy project will be used to deploy hello-world-api-demo to apigee edge. 

How to clone the hello-world-api-demo and change the directory 

```bash

# clone hello-world-api-demo repository
git clone https://bitbucket.org/sidgs/hello-world-api-demo.git

# change directory into the repository
cd hello-world-api-demo
```


## Lifecycle for API proxy deployment 


### Lint Proxy

```
# invoke apigeelint directly
apigeelint -s edge/apiproxy -f html.js

# invoke linting through maven; run from /edge directory in project root
cd edge
mvn test -Pproxy-linting             
```
 
### Unit Tests

run this from `edge` directory:
```bash
mvn test -Pproxy-unit-test
```
 
### Bundle Package

run this from `edge` directory:
```bash
mvn package \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
    -DvhostProtocol=https \
    -DvhostDomainName=brcm-cnb-sep.api.saas.broadcom.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=brcm-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration 
```
 
### Upload Caches

run this from `edge` directory:
```bash
mvn apigee-config:caches \
    -Papigee  \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
	
    
```
 
### Upload Target Servers

run this from `edge` directory:
```bash
mvn apigee-config:targetservers \
    -Papigee  \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
```
 
### Upload KVM's

run this from `edge` directory:
```bash
mvn apigee-config:keyvaluemaps \
    -Papigee  \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dusername=%APIGEE_USER% \
	-Dpassword=%APIGEE_PWD% \
    
```
 
### Deploy Proxy

run this from `edge` directory:
```bash
	
mvn apigee-enterprise:deploy \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
	-Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
	
    
```
 
### Upload Products

run this from `edge` directory:
```bash
mvn apigee-config:apiproducts \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration  \
	-Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
    
    
```
 
### Upload Developers

run this from `edge` directory:
```bash
mvn apigee-config:developers \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
	-Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration  \
	-Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
    
```
 
### Upload Apps

run this from `edge` directory:
```bash
mvn apigee-config:apps \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration  \
	-Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
    
```
 
### Export Keys

run this from `edge` directory:
```bash
mvn apigee-config:exportAppKeys \
    -Papigee \
    -Denv=sep \
    -Dorg=brcm-cnb \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration  \
	-Dusername=$APIGEE_USER \
	-Dpassword=$APIGEE_PWD \
   
```
 