pipeline {
    agent any
    tools {
        maven 'Maven'
        nodejs 'NodeJS'
    }
    stages {
        stage('Clean') {
            steps {
                dir('edge') {
                    sh "mvn clean"
                }
            }
        }

        stage('Static Code Analysis, Unit Test and Coverage') {
            steps {
                dir('edge') {
					echo ""
                    sh "mvn test -Pproxy-unit-test "
                    sh "node node_modules/istanbul/lib/cli.js cover " +
                            "--dir target/unit-test-coverage " +
                            "node_modules/mocha/bin/_mocha test/unit"
                    echo "publish html report"
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : 'target/unit-test-coverage/lcov-report',
                            reportFiles          : 'index.html',
                            reportName           : 'Code Coverage HTML Report'
                    ])
                }
            }
        }


        stage('Linting') {
            steps {
                dir("edge") {
                    sh "apigeelint -s apiproxy -f html.js > target/unit-test-coverage/apigeelint.html"
                    echo "publish html report"
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : 'target/unit-test-coverage',
                            reportFiles          : 'apigeelint.html',
                            reportName           : 'Linting HTML Report'
                    ])
                }
            }
        }

        stage('Build proxy bundle') {
            steps {
                def apigeeProps = readJSON file: './deploy-manifest.json'
                dir('edge') {

                    withCredentials([usernamePassword(credentialsId: "brcm-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {

                        sh "mvn package -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org}"+
                                " -Dusername=${apigee_user} -Dpassword=${apigee_pwd} "
                    }
                }
            }

        }
        stage('Pre-Deployment Configuration ') {
            steps {
                dir('edge') {
                    println "Predeployment of Caches "
                    withCredentials([usernamePassword(credentialsId: "brcm-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {
                        script {
                            if (fileExists("resources/edge/env/${params.apigee_env}/caches.json")) {
                                sh "mvn apigee-config:caches " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "
                            }

                            if (fileExists("resources/edge/env/${params.apigee_env}/kvms.json")) {
                                sh "mvn apigee-config:keyvaluemaps " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "
                            }

                            if (fileExists("resources/edge/env/${params.apigee_env}/targetServers.json")) {
                                sh "mvn apigee-config:targetservers " +
                                        "    -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org} " +
                                        "    -Dusername=${apigee_user} " +
                                        "    -Dpassword=${apigee_pwd} "
                            }
                        }
                    }
                }
            }
        }
        stage('Deploy proxy bundle') {
            steps {
                dir('edge') {
                    withCredentials([usernamePassword(credentialsId: "brcm-edge-cred",
                            passwordVariable: 'apigee_pwd',
                            usernameVariable: 'apigee_user')]) {

                        sh "mvn apigee-enterprise:deploy -Papigee -Denv=${params.apigee_env} -Dorg=${params.apigee_org}" +
                                " -Doptions=update -Dusername=${apigee_user} -Dpassword=${apigee_pwd} " //+
                                //" -Dbearer= " + //+"-Drefresh=${refresh} " +
                                //" -Dauthtype=basic "
                                //+ " -Dtokenurl=${tokenurl} "
                    }
                }
            }
        }
    }
}

