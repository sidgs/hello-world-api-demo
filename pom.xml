<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.brcm.api.commons</groupId>
    <artifactId>api-parent-pom</artifactId>
    <packaging>pom</packaging>
    <version>1.0.0-SNAPSHOT</version>
    <name>api-parent-pom</name>


    <pluginRepositories>
        <pluginRepository>
            <id>central</id>
            <name>Maven Plugin Repository</name>
            <url>https://repo1.maven.org/maven2</url>
            <layout>default</layout>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <updatePolicy>never</updatePolicy>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <properties>
        <!--maven.deploy.skip - Revert back to true if developer wants to do local developement and wants to skip the artifacts deployment to nexus-->
        <maven.deploy.skip>false</maven.deploy.skip>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <project.build.resourceEncoding>UTF-8</project.build.resourceEncoding>
        <maven.compile.encoding>UTF-8</maven.compile.encoding>
        <org.slf4j.simpleLogger.defaultLogLevel>info</org.slf4j.simpleLogger.defaultLogLevel>
        <project.root.dir>${basedir}</project.root.dir>
        <target.root.dir>${basedir}/target</target.root.dir>
        <deployment.suffix>${user.name}</deployment.suffix>
        <deployment.env>-${env}</deployment.env>



        <apigee.api.protocol>https</apigee.api.protocol>
        <apigee.api.host>api.enterprise.apigee.com</apigee.api.host>
        <apigee.apiversion>v1</apigee.apiversion>
        <apigee.org>${org}</apigee.org>
        <apigee.env>${env}</apigee.env>
        <apigee.tokenurl>${tokenurl}</apigee.tokenurl>
        <apigee.authtype>${authtype}</apigee.authtype>
        <apigee.bearer>${bearer}</apigee.bearer>
        <apigee.refresh>${refresh}</apigee.refresh>


        <vhostProtocol>https</vhostProtocol>
        <vhostDomainName>${org}-${env}.api.saas.broadcom.com</vhostDomainName>
        <vhostEdgeName>secure</vhostEdgeName>

        <jgitflow.version>1.0-m5.1</jgitflow.version>


        <commit>${user.name}-local</commit>
        <branch>${user.name}-local</branch>

        <scmUsername/>
        <scmPassword/>

        <!--Release repo URL for New Nexus -->
        <releases.distribution.url>${repo.server.protocol}://${repo.server.root}/${repo.path}/${repo.name}release/
        </releases.distribution.url>
        <!--Snapshot repo URL for New Nexus -->
        <snapshot.distribution.url>${repo.server.protocol}://${repo.server.root}/${repo.path}/${repo.name}snapshot/
        </snapshot.distribution.url>



    </properties>

    <distributionManagement>
        <snapshotRepository>
            <id>apiSnapshotRepository</id>
            <name>Internal Snapshots Repository</name>
            <url>${snapshot.distribution.url}</url>
            <uniqueVersion>false</uniqueVersion>
        </snapshotRepository>
    </distributionManagement>


    <profiles>

        <!--apigee-->
        <profile>
            <id>apigee</id>
            <properties>
                <apigee.profile>apigee</apigee.profile>
                <apigee.env>${env}</apigee.env>
                <apigee.hosturl>${apigee.api.protocol}://${apigee.api.host}</apigee.hosturl>
                <apigee.org>${org}</apigee.org>
                <apigee.username>${username}</apigee.username>
                <apigee.password>${password}</apigee.password>
                <apigee.options>update</apigee.options>
                <api.northbound.domain>${vhostProtocol}://${vhostDomainName}</api.northbound.domain>
                <api.testtag>~@wip</api.testtag>
                <apigee.config.dir>target/resources/edge</apigee.config.dir>
                <apigee.config.options>update</apigee.config.options>
                <apigee.override.delay>2</apigee.override.delay>
                <apigee.config.exportDir>target/test/integration</apigee.config.exportDir>
                <apigee.tokenurl>${tokenurl}</apigee.tokenurl>
                <apigee.authtype>${authtype}</apigee.authtype>
                <apigee.bearer>${bearer}</apigee.bearer>
                <apigee.refresh>${refresh}</apigee.refresh>


            </properties>

            <build>
                <plugins>
                    <!--Google replacer-->
                    <plugin>
                        <groupId>com.google.code.maven-replacer-plugin</groupId>
                        <artifactId>replacer</artifactId>
                        <version>1.5.3</version>
                        <executions>
                            <execution>
                                <phase>prepare-package</phase>
                                <goals>
                                    <goal>replace</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <ignoreErrors>true</ignoreErrors>
                            <basedir>${target.root.dir}</basedir>
                            <includes>
                                <include>apiproxy/proxies/default.xml</include>
                                <include>apiproxy/targets/default.xml</include>
                                <include>test/**/test-config.json</include>
                                <include>test/integration/auth-server.json</include>
                                <include>apiproxy/${project.artifactId}-v1.xml</include>
                                <include>resources/edge/org/apiProducts.json</include>
                                <include>resources/edge/org/developerApps.json</include>
                                <!-- files to change base path -->
                                <include>apiproxy/${project.artifactId}.xml</include>
                            </includes>
                            <replacements>
                                <replacement>
                                    <token>/${project.artifactId}/v1</token>
                                    <value>/${project.artifactId}/v1</value>
                                </replacement>
                                <replacement>
                                    <token>demo-test.apigee.net</token>
                                    <value>${api.northbound.domain}</value>
                                </replacement>
                                <replacement>
                                    <token>sampleAPIProduct</token>
                                    <value>${project.artifactId}Product-${env}</value>
                                </replacement>
                                <replacement>
                                    <token>sampleAPIApp</token>
                                    <value>${project.artifactId}-${org}-${env}</value>
                                </replacement>

                                <replacement>
                                    <token>edge_env</token>
                                    <value>${env}</value>
                                </replacement>
                                <replacement>
                                    <token>@edgeVhostName@</token>
                                    <value>${vhostEdgeName}</value>
                                </replacement>
                                <replacement>
                                    <token>@description</token>
                                    <value>commit ${commit} from ${branch} branch by ${user.name}</value>
                                </replacement>
                                <!-- To change the basepath -->
                                <replacement>
                                    <token>${actualBasePath}</token>
                                    <value>${replaceBasePath}</value>
                                </replacement>
                            </replacements>
                        </configuration>
                    </plugin>

                    <!--configure maven artifact -->
                    <plugin>
                        <artifactId>maven-antrun-plugin</artifactId>
                        <version>1.8</version>
                        <executions>
                            <execution>
                                <phase>prepare-package</phase>
                                <configuration>
                                    <target>
                                        <copy toDir="${target.root.dir}/classes">
                                            <fileset dir="${target.root.dir}">
                                                <include name="apiproxy/**"/>
                                            </fileset>
                                        </copy>
                                    </target>
                                </configuration>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>


                    <!--proxy deploy -->
                    <plugin>
                        <groupId>io.apigee.build-tools.enterprise4g</groupId>
                        <artifactId>apigee-edge-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>configure-bundle-step</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>configure</goal>
                                </goals>
                            </execution>
                            <!--deploy bundle -->
                            <execution>
                                <id>deploy-bundle-step</id>
                                <phase>install</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!--edge env and org config -->
                    <plugin>
                        <groupId>com.apigee.edge.config</groupId>
                        <artifactId>apigee-config-maven-plugin</artifactId>
                        <version>1.2.2</version>
                        <executions>
                            <execution>
                                <id>create-config-cache</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>caches</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-kvm</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>keyvaluemaps</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-targetserver</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>targetservers</goal>
                                </goals>
                            </execution>
                        
                            <execution>
                                <id>create-config-apiproduct</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>apiproducts</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-developer</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>developers</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-app</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>apps</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>export-config-app</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>exportAppKeys</goal>
                                </goals>
                            </execution>
                
                        </executions>
                    </plugin>
                </plugins>
            </build>

        </profile>

        <!-- proxy-linting -->
        <profile>
            <id>proxy-linting</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.3.2</version>
                        <executions>

                            <!-- npm install -->
                            <execution>
                                <id>npm-install</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>npm</executable>
                                    <commandlineArgs>
                                        install
                                    </commandlineArgs>
                                </configuration>
                            </execution>

                            <!-- run linting -->
                            <execution>
                                <id>proxy-linting</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>node</executable>
                                    <commandlineArgs>
                                        node_modules/apigeelint/cli.js -s apiproxy -f html.js > apigeelint.html
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- proxy-integration-init -->
        <profile>
            <id>proxy-integration-test</id>
            <build>
                <plugins>


                        <plugin>
                            <artifactId>maven-antrun-plugin</artifactId>
                            <version>1.8</version>
                            <executions>
                                <execution>
                                    <phase>compile</phase>
                                    <configuration>
                                        <target>
                                            <replace dir="${project.build.directory}/resources/edge"
                                                     token="#{artifactId}" value="${artifactId}">
                                                <include name="**/*.json"/>
                                            </replace>
                                            <replace dir="${project.build.directory}/resources/edge"
                                                     token="#{env}" value="${env}">
                                                <include name="**/*.json"/>
                                            </replace>
                                        </target>
                                    </configuration>
                                    <goals>
                                        <goal>run</goal>
                                    </goals>
                                </execution>
                            </executions>
                        </plugin>


                    <plugin>
                        <groupId>com.apigee.edge.config</groupId>
                        <artifactId>apigee-config-maven-plugin</artifactId>
                        <version>1.2.2</version>
                        <executions>
                            <execution>
                                <id>create-config-apiproduct</id>
                                <phase>test-compile</phase>
                                <goals>
                                    <goal>apiproducts</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-developer</id>
                                <phase>test-compile</phase>
                                <goals>
                                    <goal>developers</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>create-config-app</id>
                                <phase>test-compile</phase>
                                <goals>
                                    <goal>apps</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>export-config-app</id>
                                <phase>test-compile</phase>
                                <goals>
                                    <goal>exportAppKeys</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <!--functional init -->
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.3.2</version>
                        <executions>
                            <!-- npm install -->
                            <execution>
                                <id>npm-install</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>npm</executable>
                                    <commandlineArgs>
                                        install
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                            <!-- run integration tests -->
                            <execution>
                                <id>integration</id>
                                <phase>install</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>node</executable>
                                    <commandlineArgs>
                                        node_modules/cucumber/bin/cucumber.js target/test/integration/features --format
                                        json:target/reports.json
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>

            </build>
        </profile>

        <!-- proxy-unit-init -->
        <profile>
            <id>proxy-unit-test</id>
            <build>
                <plugins>

                    <!-- run jslint -->
                    <plugin>
                        <groupId>com.cj.jshintmojo</groupId>
                        <artifactId>jshint-maven-plugin</artifactId>
                        <version>1.6.0</version>
                        <executions>
                            <execution>
                                <id>jslint</id>
                                <phase>validate</phase>
                                <goals>
                                    <goal>lint</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <directories>
                                <directory>apiproxy/resources/jsc</directory>
                            </directories>
                            <reporter>jslint</reporter>
                        </configuration>
                    </plugin>

                    <!--unit init , code coverage , functional init -->
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>exec-maven-plugin</artifactId>
                        <version>1.3.2</version>
                        <executions>
                            <!-- npm install -->
                            <execution>
                                <id>npm-install</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>npm</executable>
                                    <commandlineArgs>
                                        install
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                            <!-- run unit tests -->
                            <execution>
                                <id>unit</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>node</executable>
                                    <commandlineArgs>
                                        node_modules/istanbul/lib/cli.js cover --dir target/unit-test-coverage
                                        node_modules/mocha/bin/_mocha test/unit
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                            <!-- Check Code Coverage-->
                            <execution>
                                <id>check-coverage</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>exec</goal>
                                </goals>
                                <configuration>
                                    <executable>node</executable>
                                    <commandlineArgs>
                                        node_modules/istanbul/lib/cli.js check-coverage
                                    </commandlineArgs>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                </plugins>

            </build>
        </profile>

        <profile>
            <id>developer</id>
               <properties>    
               <!-- Default Properties for Developers -->
               <env>sep</env>
               <org>bcrm-cnb</org>
               <!-- <apigee.tokenurl>https://login.apigee.com/oauth/token</apigee.tokenurl> -->
               <apigee.authtype>oauth</apigee.authtype>
               <edgeVhostName>secure</edgeVhostName>
               </properties>
        
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>properties-maven-plugin</artifactId>
                        <version>1.0.0</version>
                        <executions>
                            <execution>
                                <phase>initialize</phase>
                                <goals>
                                    <goal>read-project-properties</goal>
                                </goals>
                                <configuration>
                                    <files>
                                        <file>${basedir}/my-tokens.properties</file>
                                    </files>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>apigee-prep</id>
            <properties>
                <apigee.profile>apigee-prep</apigee.profile>
                <apigee.env>${env}</apigee.env>
                <apigee.hosturl>${apigee.api.protocol}://${apigee.api.host}</apigee.hosturl>
                <apigee.org>${org}</apigee.org>
                <apigee.username>${username}</apigee.username>
                <apigee.password>${password}</apigee.password>
                <apigee.options>clean</apigee.options>
                <api.northbound.domain>${vhostProtocol}://${vhostDomainName}</api.northbound.domain>
                <api.testtag>~@wip</api.testtag>
                <apigee.config.dir>target/resources/edge</apigee.config.dir>
                <apigee.config.options>delete</apigee.config.options>
                <apigee.override.delay>2</apigee.override.delay>
                <apigee.config.exportDir>target/test/integration</apigee.config.exportDir>
                <apigee.tokenurl>${tokenurl}</apigee.tokenurl>
                <apigee.authtype>${authtype}</apigee.authtype>
                <apigee.bearer>${bearer}</apigee.bearer>
                <apigee.refresh>${refresh}</apigee.refresh>
            </properties>

            <build>
                <plugins>
                  <!--proxy deploy -->
                    <plugin>
                        <groupId>io.apigee.build-tools.enterprise4g</groupId>
                        <artifactId>apigee-edge-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>configure-bundle-step</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>configure</goal>
                                </goals>
                            </execution>
                            <!--deploy bundle -->
                            <execution>
                                <id>deploy-bundle-step</id>
                                <phase>install</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                 </plugins>
            </build>

        </profile>

    </profiles>

    <build>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>io.apigee.build-tools.enterprise4g</groupId>
                    <artifactId>apigee-edge-maven-plugin</artifactId>
                    <version>1.1.7</version>
                </plugin>
                <plugin>
                    <groupId>com.apigee.edge.config</groupId>
                    <artifactId>apigee-config-maven-plugin</artifactId>
                    <version>1.2.2</version>
                </plugin>
            </plugins>
        </pluginManagement>

    </build>


</project>
